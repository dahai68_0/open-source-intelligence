# 思通数科开源情报系统-(StoneOSI)

## 介绍
开源情报系统是面向AI的“数据汇聚”、“数据标记”和“数据应用”三方面核心能力，思通数科开源情报系统连通底层基础设施与上层应用软件，为各类数据开发与数据应用保驾护航，为用户提供高实时、高性能、高可用的数据时支撑。


## 开源情报系统 的功能：
    
-   信源管理：针对不同采集内容管理，根据不同类型添加采集信源，实时监测采集信息源状态；
-   数据采集：数据采集通过拖拽与低代码开发，实现网页、论坛、公众号、APP、短视频据采集，集成反爬虫策略;
-   日志管理：通过“跟踪ID”对数据采集和数据处理全流程进行监控，让用户对全天候的系统运作情况了如指掌;
-   标签管理：用户可以通过标签配置对不同类型的数据，配置不同的标签标记;
-   接口管理：数据采集完毕后，用户可以自定义数据对外输出接口;


## 开源情报系统 的优势：

-   开源开放：零门槛，线上快速获取和安装；快速获取用户反馈、按月发布新版本；
-   简单易用：极易上手，不懂网络爬虫技术，也可轻松采集海量数据；
-   秒级响应：每日采集超大数据量下秒级查询返回；


## 开源情报系统 技术栈：

- 数据库：MySQL
- 数据检索：Elasticsearch
- 文章储存：Mongodb
- 系统缓存：Redis
- 消息队列：kafak & rabbitMQ
- 深度学习：PaddlePaddle
- 网络爬虫：WebMagic(java) & scrapy（python）
- 开发框架：SpringBoot
- 开发语言：Java Vue


## UI 展示 
![输入图片说明](proIMG/2022-02-15-mianpage.png)
![输入图片说明](proIMG/datahouse.png)
![输入图片说明](proIMG/seed.png)
![输入图片说明](proIMG/spider.png)
![输入图片说明](proIMG/log.png)



## 在线体验

-   环境地址：<http://s1.stonedt.com:5366/>
-   用户名：13900000000
-   密码：stonedt


## 开源计划
- 项目初期提供，前端开源web产品界面。
- 项目中期提供，后端开源java分布式采集系统。
- 项目后期提供，后端分布式安装说明文档。


## 开源说明
- 本项目为开源项目，支持本地化部署，用户需要搭建后端分布式集群，本项目在文档有相关说明，提供安装文档。
- 也提供商业版本，与开源版本区分于后端分布式的水平扩展，以及商业化数据采集服务。


## 微信
   扫描微信二维码，技术交流。

<img src="https://gitee.com/stonedtx/yuqing/raw/master/ProIMG/%E8%81%94%E7%B3%BB%E6%88%91%E4%BB%AC-%E4%B8%AA%E4%BA%BA%E5%BE%AE%E4%BF%A1.jpg" title="Logo"  width="220">


## 联系我们

+ 微信号： techflag  

+ 电话： 13505146123

+ 邮箱： wangtao@stonedt.com

+ 公司官网：[www.stonedt.com](http://www.stonedt.com)

欢迎您在下方留言，或添加微信与我们交流。


## License & Copyright

Copyright (c) 2014-2022 思通数科 StoneDT, All rights reserved.

Licensed under The GNU General Public License version 3 (GPLv3)  (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

<https://www.gnu.org/licenses/gpl-3.0.html>

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.