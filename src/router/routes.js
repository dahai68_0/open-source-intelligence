import store from '@/state/store'

export default [
    {
        path: '/login',
        name: 'login',
        // component: () => import('../views/pages/account/login'),
        component: () => import('../views/pages/login/login'),
        // meta: {
        //     beforeResolve(routeTo, routeFrom, next) {
        //         // If the user is already logged in
        //         if (store.getters['auth/loggedIn']) {
        //             // Redirect to the home page instead
        //             next({ name: 'home' })
        //         } else {
        //             // Continue to the login page
        //             next()
        //         }
        //     },
        // },
    },
    // 开放源代码许可
    {
        path: '/license',
        name: 'license',
        component: () => import('../views/pages/login/license')
    },
    {
        path: '/register',
        name: 'register',
        component: () => import('../views/pages/account/register'),
        meta: {
            beforeResolve(routeTo, routeFrom, next) {
                // If the user is already logged in
                if (store.getters['auth/loggedIn']) {
                    // Redirect to the home page instead
                    next({ name: 'home' })
                } else {
                    // Continue to the login page
                    next()
                }
            },
        },
    },
    {
        path: '/forgot-password',
        name: 'Forgot-password',
        component: () => import('../views/pages/account/forgot-password'),
        meta: {
            beforeResolve(routeTo, routeFrom, next) {
                // If the user is already logged in
                if (store.getters['auth/loggedIn']) {
                    // Redirect to the home page instead
                    next({ name: 'home' })
                } else {
                    // Continue to the login page
                    next()
                }
            },
        },
    },
    {
        path: '/logout',
        name: 'logout',
        meta: {
            authRequired: true,
            beforeResolve(routeTo, routeFrom, next) {
                if (process.env.VUE_APP_DEFAULT_AUTH === "firebase") {
                    store.dispatch('auth/logOut')
                } else {
                    store.dispatch('authfack/logout')
                }
                const authRequiredOnPreviousRoute = routeFrom.matched.some(
                    (route) => route.push('/login')
                )
                // Navigate back to previous page, or home as a fallback
                next(authRequiredOnPreviousRoute ? { name: 'home' } : { ...routeFrom })
            },
        },
    },
    {
        path: '/',
        name: 'login',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/login/login')
    },
    // 首页
    {
        path: '/dashboard',
        name: 'dashboard',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/dashboard/index')
    },
    // 系统运行报告列表
    {
        path: '/dashboard/operationReport',
        name: 'operationReport',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/dashboard/operationReport')
    },
    // 系统运行报告详情
    {
        path: '/dashboard/operationReportDetail',
        name: 'operationReportDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/dashboard/operationReportDetail')
    },
    // 信源生命周期
    {
        path: '/dashboard/sourceLifeCycle',
        name: 'sourceLifeCycle',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/dashboard/sourceLifeCycle')
    },
    // 数据生命周期
    {
        path: '/dashboard/dataLifeCycle',
        name: 'dataLifeCycle',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/dashboard/dataLifeCycle')
    },
    // 信源管理
    {
        path: '/seed',
        name: 'seed',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/index')
    },
    //添加信源
    {
        path: '/seed/addSeed',
        name: 'AddSeed',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/AddSeed')
    },
    //信源详情
    {
        path: '/seed/seedDetail',
        name: 'seedDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/seedDetail')
    },
    //seed1
    {
        path: '/seed1',
        name: 'Seed1',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/seed1/Seed1')
    },
    //seed2
    {
        path: '/seed2',
        name: 'Seed2',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/seed2/Seed2')
    },
    //seed3
    {
        path: '/seed3',
        name: 'Seed3',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/seed3/Seed3')
    },
    //seed4
    {
        path: '/seed4',
        name: 'Seed4',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/seed4/Seed4')
    },
    //seed5
    {
        path: '/seed5',
        name: 'Seed5',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/seed5/Seed5')
    },
    //seed6
    {
        path: '/seed6',
        name: 'Seed6',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/seed6/Seed6')
    },
    //seed7
    {
        path: '/seed7',
        name: 'Seed7',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/seed7/Seed7')
    },
    //seed8
    {
        path: '/seed8',
        name: 'Seed8',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/seed8/Seed8')
    },
    //seed9
    {
        path: '/seed9',
        name: 'Seed9',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/seed9/Seed9')
    },
    //log2
    {
        path: '/log2',
        name: 'log2',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/log/log2')
    },


    // 数据采集
    {
        path: '/spider',
        name: 'spider',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/spider/index')
    },
    // 采集模板
    {
        path: '/spider/managementTemplate',
        name: 'managementTemplate',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/spider/managementTemplate')
    },
    // 创建模板
    {
        path: '/spider/createTemplate',
        name: 'createTemplate',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/spider/createTemplate')
    },
    // 站点详情
    {
        path: '/spider/siteDetail',
        name: 'siteDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/spider/siteDetail')
    },
    // 自媒体号详情
    {
        path: '/spider/weMediaDetail',
        name: 'weMediaDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/spider/weMediaDetail')
    },
    // 添加自媒体号
    {
        path: '/spider/addWeMedia',
        name: 'addWeMedia',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/spider/addWeMedia')
    },
    // 批量导入工商数据
    {
        path: '/spider/importBus',
        name: 'importBus',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/spider/importBus')
    },
    // 日志管理
    {
        path: '/log',
        name: 'log',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/log/index')
    },
    // 日志详情
    {
        path: '/log/pageDetail',
        name: 'pageDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/log/pageDetail')
    },
    // 标签管理
    {
        path: '/lable',
        name: 'lable',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/lable/index')
    },
    // 编辑资讯标签
    {
        path: '/lable/editLabel',
        name: 'editLabel',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/lable/editLabel')
    },
    // 添加标签
    {
        path: '/lable/addLabel',
        name: 'addLabel',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/lable/addLabel')
    },
    // 资讯标记数据
    {
        path: '/lable/signList',
        name: 'signList',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/lable/signList')
    },
    // 查看和修改标记数据
    {
        path: '/lable/signDetail',
        name: 'signDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/lable/signDetail')
    },
    // 数仓管理
    {
        path: '/datahouse',
        name: 'datahouse',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/index')
    },
    // 数仓管理  资讯详情
    {
        path: '/datahouse/infoDetail',
        name: 'infoDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/infoDetail')
    },
    // 数仓管理  招标详情
    {
        path: '/datahouse/bidDetail',
        name: 'bidDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/bidDetail')
    },
    // 数仓管理  工商详情
    {
        path: '/datahouse/businessDetail',
        name: 'businessDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/businessDetail')
    },
    // 数仓管理  招聘详情
    {
        path: '/datahouse/recruitDetail',
        name: 'recruitDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/recruitDetail')
    },
    // 数仓管理  政策详情
    {
        path: '/datahouse/policyDetail',
        name: 'policyDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/policyDetail')
    },
    // 数仓管理  自媒体号详情
    {
        path: '/datahouse/weMediaNumDetail',
        name: 'weMediaNumDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/weMediaNumDetail')
    },
    // 数仓管理  自媒体文章详情
    {
        path: '/datahouse/weMediaArticleDetail',
        name: 'weMediaArticleDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/weMediaArticleDetail')
    },
    // 数仓管理  上市公司详情
    {
        path: '/datahouse/securitieslistCompanyDetail',
        name: 'securitieslistCompanyDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/securitieslistCompanyDetail')
    },
    // 数仓管理  公司公告详情
    {
        path: '/datahouse/securitiesAnnouncementDetail',
        name: 'securitiesAnnouncementDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/securitiesAnnouncementDetail')
    },
    // 数仓管理   数仓实例
    {
        path: '/datahouse/example',
        name: 'example',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/example/index')
    },
    // 数仓管理   数仓实例  创建数据实例
    {
        path: '/datahouse/addExample',
        name: 'addExample',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/example/addExample')
    },
    // 数仓管理   数仓实例  创建数据实例
    {
        path: '/datahouse/addSurface',
        name: 'addSurface',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/example/addSurface')
    },
    // 数仓管理   数仓实例  数据表结构
    {
        path: '/datahouse/dataField',
        name: 'dataField',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/example/dataField')
    },
    // 数仓管理   数仓实例  数据表数据
    {
        path: '/datahouse/dataDetail',
        name: 'dataDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/example/dataDetail')
    },
    // 数仓管理   数仓实例  创建数据源
    {
        path: '/datahouse/addDataSource',
        name: 'addDataSource',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/example/addDataSource')
    },
    // 数仓管理   数据处理
    {
        path: '/datahouse/processing',
        name: 'processing',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/processing/index')
    },
    // 数仓管理   数据处理  创建数据处理脚本
    {
        path: '/datahouse/addScript',
        name: 'addScript',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/processing/addScript')
    },
    // 数仓管理   数据融合
    {
        path: '/datahouse/fuse',
        name: 'fuse',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/fuse/index')
    },
    // 数仓管理   创建融合任务
    {
        path: '/datahouse/newTask',
        name: 'newTask',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/datahouse/fuse/newTask')
    },
    // 计算资源
    {
        path: '/computing',
        name: 'computing',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/computing/index')
    },
    // 计算资源  添加应用服务器监控
    {
        path: '/computing/addApplication',
        name: 'addApplication',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/computing/addApplication')
    },
    // 计算资源  应用服务器详情
    {
        path: '/computing/applicationDetail',
        name: 'applicationDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/computing/applicationDetail')
    },
    // 计算资源  添加虚拟服务器监控
    {
        path: '/computing/addFictitious',
        name: 'addFictitious',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/computing/addFictitious')
    },
    // 计算资源  虚拟服务器详情
    {
        path: '/computing/fictitiousDetail',
        name: 'fictitiousDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/computing/fictitiousDetail')
    },
    // 计算资源  添加真实服务器监控
    {
        path: '/computing/addReal',
        name: 'addReal',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/computing/addReal')
    },
    // 计算资源  真实服务器详情
    {
        path: '/computing/realDetail',
        name: 'fictitiousDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/computing/realDetail')
    },
    // 客户管理
    {
        path: '/custom',
        name: 'custom',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/custom/index')
    },
    // 客户管理详情页
    {
        path: '/custom/detail',
        name: 'customDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/custom/detail')
    },
    // 接口管理
    {
        path: '/interface',
        name: 'interface',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/interface/index')
    },
    // 添加项目
    {
        path: '/interface/creatProject',
        name: 'creatProject',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/interface/creatProject')
    },
    // 爬虫
    {
        path: '/spider_detail/:flowId',
        name: 'spider_detail',
        component: () => import('../views/pages/spiderflow/index'),
    },
    {
        path: '/chat',
        name: 'Chat',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/chat/index')
    },
    {
        path: '/seed',
        name: 'seed',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/index')
    },
    {
        path: '/addSeed',
        name: 'AddSeed',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/seed/AddSeed')
    },
    // 用户管理
    {
        path: '/user',
        name: 'user',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/user/index')
    },
    // 添加用户
    {
        path: '/user/addUser',
        name: 'addUser',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/user/addUser')
    },
    // 用户中心
    {
        path: '/user/userCenter',
        name: 'userCenter',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/user/userCenter')
    },
    // 用户详情
    {
        path: '/user/userDetail',
        name: 'userDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/user/userDetail')
    },
    // 角色管理
    {
        path: '/user/roleManagement',
        name: 'roleManagement',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/user/roleManagement')
    },
    // 角色详情
    {
        path: '/user/roleDetail',
        name: 'roleDetail',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/user/roleDetail')
    },
    {
        path: '/calendar',
        name: 'Calendar',
        meta: {
            authRequired: true,
        },
        component: () => import('../views/pages/calendar/index')
    },
    {
        path: '/ecommerce/products',
        name: 'products',
        meta: { authRequired: true },
        component: () => import('../views/pages/ecommerce/products')
    },
    {
        path: '/ecommerce/product-detail',
        name: 'product detail',
        meta: { authRequired: true },
        component: () => import('../views/pages/ecommerce/product-detail')
    },
    {
        path: '/ecommerce/orders',
        name: 'Orders',
        meta: { authRequired: true },
        component: () => import('../views/pages/ecommerce/orders')
    },
    {
        path: '/ecommerce/customers',
        name: 'Customers',
        meta: { authRequired: true },
        component: () => import('../views/pages/ecommerce/customers')
    },
    {
        path: '/ecommerce/cart',
        name: 'Cart',
        meta: { authRequired: true },
        component: () => import('../views/pages/ecommerce/cart')
    },
    {
        path: '/ecommerce/checkout',
        name: 'Checkout',
        meta: { authRequired: true },
        component: () => import('../views/pages/ecommerce/checkout')
    },
    {
        path: '/ecommerce/shops',
        name: 'Shops',
        meta: { authRequired: true },
        component: () => import('../views/pages/ecommerce/shops')
    },
    {
        path: '/ecommerce/add-product',
        name: 'Add-product',
        meta: { authRequired: true },
        component: () => import('../views/pages/ecommerce/add-product')
    },
    {
        path: '/email/inbox',
        name: 'Inbox',
        meta: { authRequired: true },
        component: () => import('../views/pages/email/inbox')
    },
    {
        path: '/email/read',
        name: 'Read Email',
        meta: { authRequired: true },
        component: () => import('../views/pages/email/reademail')
    },
    {
        path: '/auth/login-1',
        name: 'login-1',
        meta: { authRequired: true },
        component: () => import('../views/pages/sample-auth/login-1')
    },
    {
        path: '/auth/register-1',
        name: 'register-1',
        meta: { authRequired: true },
        component: () => import('../views/pages/sample-auth/register-1')
    },
    {
        path: '/auth/lock-screen-1',
        name: 'Lock-screen-1',
        meta: { authRequired: true },
        component: () => import('../views/pages/sample-auth/lock-screen-1')
    },
    {
        path: '/auth/recoverpwd-1',
        name: 'Recoverpwd-1',
        meta: { authRequired: true },
        component: () => import('../views/pages/sample-auth/recoverpwd-1')
    },
    {
        path: '/apps/kanban-board',
        name: 'Kanban-board',
        meta: { authRequired: true },
        component: () => import('../views/pages/kanbanboard/index')
    },
    {
        path: '/pages/starter',
        name: 'Starter Page',
        meta: { authRequired: true },
        component: () => import('../views/pages/utility/starter')
    },
    {
        path: '/pages/maintenance',
        name: 'Maintenance',
        meta: { authRequired: true },
        component: () => import('../views/pages/utility/maintenance')
    },
    {
        path: '/pages/coming-soon',
        name: 'Coming-soon',
        meta: { authRequired: true },
        component: () => import('../views/pages/utility/coming-soon')
    },
    {
        path: '/pages/timeline',
        name: 'Timeline',
        meta: { authRequired: true },
        component: () => import('../views/pages/utility/timeline/index')
    },
    {
        path: '/pages/faqs',
        name: 'Faqs',
        meta: { authRequired: true },
        component: () => import('../views/pages/utility/faqs')
    },
    {
        path: '/pages/pricing',
        name: 'Pricing',
        meta: { authRequired: true },
        component: () => import('../views/pages/utility/pricing/index')
    },
    {
        path: '/pages/error-404',
        name: 'Error-404',
        meta: { authRequired: true },
        component: () => import('../views/pages/utility/error-404')
    },
    {
        path: '/pages/error-500',
        name: 'Error-500',
        meta: { authRequired: true },
        component: () => import('../views/pages/utility/error-500')
    },
    {
        path: '/icons/font-awesome',
        name: 'Font Awesome 5',
        meta: { authRequired: true },
        component: () => import('../views/pages/icons/font-awesome/index')
    },
    {
        path: '/icons/dripicons',
        name: 'Dripicons',
        meta: { authRequired: true },
        component: () => import('../views/pages/icons/dripicons')
    },
    {
        path: '/icons/material-design',
        name: 'Material Design',
        meta: { authRequired: true },
        component: () => import('../views/pages/icons/materialdesign/index')
    },
    {
        path: '/icons/remix',
        name: 'Remix Icons',
        meta: { authRequired: true },
        component: () => import('../views/pages/icons/remix/index')
    },
    {
        path: '/ui/buttons',
        name: 'Buttons',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/buttons')
    },
    {
        path: '/ui/alerts',
        name: 'Alerts',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/alerts')
    },
    {
        path: '/ui/grid',
        name: 'Grid',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/grid')
    },
    {
        path: '/ui/cards',
        name: 'Cards',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/cards')
    },
    {
        path: '/ui/carousel',
        name: 'Carousel',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/carousel')
    },
    {
        path: '/ui/dropdowns',
        name: 'Dropdowns',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/dropdowns')
    },
    {
        path: '/ui/images',
        name: 'Images',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/images')
    },
    {
        path: '/ui/modals',
        name: 'Modals',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/modals')
    },
    {
        path: '/ui/rangeslider',
        name: 'Range - slider',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/rangeslider')
    },
    {
        path: '/ui/progressbar',
        name: 'Progressbar',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/progressbars')
    },
    {
        path: '/ui/sweet-alert',
        name: 'Sweet-alert',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/sweet-alert')
    },
    {
        path: '/ui/tabs-accordion',
        name: 'Tabs & Accordion',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/tabs-accordions')
    },
    {
        path: '/ui/typography',
        name: 'Typography',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/typography')
    },
    {
        path: '/ui/video',
        name: 'Video',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/video')
    },
    {
        path: '/ui/general',
        name: 'General',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/general')
    },
    {
        path: '/ui/lightbox',
        name: 'Lightbox',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/lightbox')
    },
    {
        path: '/ui/notification',
        name: 'Notification',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/notification')
    },
    {
        path: '/ui/rating',
        name: 'Rating',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/rating')
    },
    {
        path: '/ui/session-timeout',
        name: 'Session Timeout',
        meta: { authRequired: true },
        component: () => import('../views/pages/ui/session-timeout')
    },
    {
        path: '/form/elements',
        name: 'Form Elements',
        meta: { authRequired: true },
        component: () => import('../views/pages/forms/elements')
    },
    {
        path: '/form/validation',
        name: 'Form validation',
        meta: { authRequired: true },
        component: () => import('../views/pages/forms/validation')
    },
    {
        path: '/form/advanced',
        name: 'Form Advanced',
        meta: { authRequired: true },
        component: () => import('../views/pages/forms/advanced')
    },
    {
        path: '/form/editor',
        name: 'CK Editor',
        meta: { authRequired: true },
        component: () => import('../views/pages/forms/ckeditor')
    },
    {
        path: '/form/uploads',
        name: 'File Uploads',
        meta: { authRequired: true },
        component: () => import('../views/pages/forms/uploads')
    },
    {
        path: '/form/wizard',
        name: 'Form Wizard',
        meta: { authRequired: true },
        component: () => import('../views/pages/forms/wizard')
    },
    {
        path: '/form/mask',
        name: 'Form Mask',
        meta: { authRequired: true },
        component: () => import('../views/pages/forms/mask')
    },
    {
        path: '/tables/basic',
        name: 'Basic Tables',
        meta: { authRequired: true },
        component: () => import('../views/pages/tables/basictable')
    },
    {
        path: '/tables/advanced',
        name: 'Advanced Tables',
        meta: { authRequired: true },
        component: () => import('../views/pages/tables/advancedtable')
    },
    {
        path: '/charts/apex',
        name: 'Apex chart',
        meta: { authRequired: true },
        component: () => import('../views/pages/charts/apex')
    },
    {
        path: '/charts/chartjs',
        name: 'Chartjs chart',
        meta: { authRequired: true },
        component: () => import('../views/pages/charts/chartjs/index')
    },
    {
        path: '/charts/chartist',
        name: 'Chartist chart',
        meta: { authRequired: true },
        component: () => import('../views/pages/charts/chartist')
    },
    {
        path: '/charts/echart',
        name: 'Echart chart',
        meta: { authRequired: true },
        component: () => import('../views/pages/charts/echart/index')
    },
    {
        path: '/maps/google',
        name: 'Google Maps',
        meta: { authRequired: true },
        component: () => import('../views/pages/maps/google')
    },
    {
        path: '/maps/leaflet',
        name: 'Leaflet Maps',
        meta: { authRequired: true },
        component: () => import('../views/pages/maps/leaflet/index')
    },
]
